import flask


app = flask.Flask(__name__)


@app.route("/add/<int:x>/<int:y>")
def add(x: int, y: int):
    return str(x + y)
