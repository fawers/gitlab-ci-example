import unittest

import ops


class TestOps(unittest.TestCase):
    def test_add(self):
        for x in range(1, 6):
            y = x + 1
            with self.subTest(x=x, y=y, **{'x+y': x+y}):
                self.assertEqual(ops.add(x, y), x + y)

    def test_sub(self):
        for x in range(14, 9, -1):
            y = x - 5
            with self.subTest(x=x, y=y, **{'x-y': x-y}):
                self.assertEqual(ops.sub(x, y), x - y)
