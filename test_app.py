import os
import unittest

from flask.testing import FlaskClient

from app import app


class TestApp(unittest.TestCase):
    @unittest.skipUnless(os.getenv("should_test"), "test only when required")
    def test_add(self):
        x, y = 2, 3

        with FlaskClient(app) as client:
            res = client.get(f"/add/{x}/{y}")
            self.assertEqual(res.data.decode(), str(x+y))
